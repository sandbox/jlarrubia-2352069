Installation
------------

1. Go to the section Administer> Blocks and choose the region where the applet
container "CCC-Graph Applet" will be displayed within the site. 

2. To allow users to access to the generated graph, assign the permission
'Access to module' to the corresponding role.

3. Go to the main configuration panel Admin > Configuration > System > CCCGraph
to customize general aspects of the graph, such as height and width of the
containing block or the graph depth.

4. From the tab "Content administration", choose (node references type and
taxonomy) fields that will be part of the conceptual structure and represented
as nodes and edges in the graph inserting an hexadecimal color to be represented.
Leave empty fields to not include them.

This step is essential because all elements of the site AREN'T considered part
of the conceptual structure by default.

PLANNED FEATURES
----------------

- Selection of type of relations.
- Responsive design of the block containing the graph.
