<?php

/**
 * @file
 * Create the body of graph.
 */

/**
 * Generates the relations graph from a given node.
 *
 * @param int $beginfrom
 *   The given node id.
 *
 * @return array
 *   An array of edges, setting the graph structure.
 */
function cccgraph_get_menu($beginfrom) {

  if (empty($beginfrom)) {
    return FALSE;
  }

  $setting_levels = variable_get('cccgraph_levels', '2');
  $visited_nodes = $novisited_nodes = $edges = $list_fields = $relations = array();
  $novisited_nodes[0] = $beginfrom;
  $pp = $level = 0;
  $next_level = 1;

  $fields = variable_get('cccgraph_visible_nodereferences', '');
  $list_fields = json_decode($fields, TRUE);
  if (empty($list_fields)) {
    $list_fields = array();
  }

  $fields_tax = variable_get('cccgraph_visible_taxonomies', '');
  $list_fields_tax = json_decode($fields_tax, TRUE);
  if (empty($list_fields_tax)) {
    $list_fields_tax = array();
  }

  // While there are related nodes and we haven´t reached the tree expansion
  // limit level.
  while (($level < $setting_levels) && (count($visited_nodes) != count($novisited_nodes))) {

    $current_nid = $novisited_nodes[$pp];
    $current_basics = db_query('SELECT title, type FROM {node} WHERE nid = :nid', array(':nid' => $current_nid))->fetchObject();

    // Relations which belongs to node_reference fields.
    foreach ($list_fields as $content_type) {
      foreach ($content_type as $field_name => $info) {
        // Relations where the current node appears as SOURCE.
        $field_info   = field_info_instance('node', $field_name, $current_basics->type);
        $field_label  = $field_info['label'];
        // DB schema information to perform de query.
        $field_name_nid = $field_name . '_nid';
        $field_table    = 'field_data_' . $field_name;
        // Get the nodes referenced by current.
        $query = db_select($field_table, 'ft');
        $query->join('node', 'n_source', 'ft.entity_id = n_source.nid');
        $query->join('node', 'n_target', "ft.$field_name_nid = n_target.nid");
        $field_info = array(
          'entity_id',
          $field_name_nid,
          'entity_type',
          'bundle',
        );
        $query->fields('ft', $field_info);
        $query->addField('n_source', 'title', 'source_title');
        $query->addField('n_target', 'title', 'target_title');
        $query->condition('ft.entity_id', $current_nid, '=');
        $query->condition('n_target.status', 1, '=');

        // Check if the field exists for this bundle.
        if (!empty($field_info)) {
          $result = $query->execute();

          while ($row = $result->fetchObject()) {
            if (!empty($row->$field_name_nid)) {
              if (!in_array($row->$field_name_nid, $visited_nodes)) {
                // Create a new graph relation.
                $new_rel = array(
                  'nid' => $row->entity_id,
                  'target' => $row->$field_name_nid,
                  'name' => t($field_label),
                  'type' => 'node_reference',
                  'color' => $info['color'],
                  'source_title' => t($row->source_title),
                  'target_title' => t($row->target_title),
                );
                array_push($relations, $new_rel);
                // Save the node as part of the tree which
                // will be iterated to get the whole relations graph.
                if (!cccgraph_in_array($row->$field_name_nid, $novisited_nodes, $pp)) {
                  $novisited_nodes[count($novisited_nodes)] = $row->$field_name_nid;
                }
              }
            }
          }
        }
        // Relations where the current node appears as TARGET.
        $where =& $query->conditions();
        unset($where[0]);
        unset($where[1]);
        // Get the nodes that reference current.
        // (Opposite direction of the relation).
        $query->condition("ft.$field_name_nid", $current_nid, '=');
        $query->condition('n_source.status', 1, '=');
        $result = $query->execute();

        while ($row = $result->fetchObject()) {
          if (!in_array($row->entity_id, $visited_nodes)) {
            $field_info = field_info_instance('node', $field_name, $row->bundle);
            $field_label = $field_info['label'];
            // Create a new graph relation.
            $new_rel = array(
              'nid' => $row->entity_id,
              'target' => $row->$field_name_nid,
              'name' => t($field_label),
              'type' => 'node_reference',
              'color' => $info['color'],
              'source_title' => t($row->source_title),
              'target_title' => t($row->target_title),
            );
            array_push($relations, $new_rel);
            // Save the node as part of the tree which
            // will be iterated to get the whole relations graph.
            if (!cccgraph_in_array($row->entity_id, $novisited_nodes, $pp)) {
              $novisited_nodes[count($novisited_nodes)] = $row->entity_id;
            }
          }
        }
      }
    }

    // Relations which belongs to taxonomy fields.
    $terms = array();
    foreach ($list_fields_tax as $content_type) {
      foreach ($content_type as $field_name => $info) {
        // Terms used to tag the current.
        $field_info   = field_info_instance('node', $field_name, $current_basics->type);
        $field_label  = $field_info['label'];
        // DB schema information to perform de query.
        $field_name_tid = $field_name . '_tid';
        $field_table    = 'field_data_' . $field_name;

        // Check if the field exists for this bundle.
        if (!empty($field_info)) {
          $query = db_select($field_table, 'ft');
          $query->join('taxonomy_term_data', 'td', "ft.$field_name_tid = td.tid");
          $query->fields('ft', array($field_name_tid));
          $query->condition('ft.entity_id', $current_nid, '=');
          $query->fields('td', array('tid', 'name'));
          $result = $query->execute();

          while ($row = $result->fetchObject()) {
            if (!empty($row->$field_name_tid)) {
              // Create a new graph relation.
              $new_rel = array(
                'nid' => $current_nid,
                'target' => $row->tid,
                'name' => t($field_label),
                'type' => 'taxonomy',
                'color' => $info['color'],
                'source_title' => t($current_basics->title),
                'target_title' => t($row->name),
              );
              array_push($relations, $new_rel);

              if (!in_array($row->$field_name_tid, $terms)) {
                array_push($terms, $row->$field_name_tid);
              }
            }
          }
        }
      }
    }
    if (!empty($terms)) {
      foreach ($list_fields_tax as $content_type) {
        foreach ($content_type as $field_name => $info) {
          // DB schema information to perform the query.
          $field_name_tid  = $field_name . '_tid';
          $field_table     = 'field_data_' . $field_name;

          // Get nodes tagged with same terms.
          $query = db_select($field_table, 'ft');
          $query->join('taxonomy_term_data', 'td', "ft.$field_name_tid = td.tid");
          $query->join('node', 'n_source', 'ft.entity_id = n_source.nid');
          $query->fields('ft', array('entity_id', $field_name_tid, 'bundle'));
          $query->fields('td', array('tid', 'name'));
          $query->addField('n_source', 'title', 'source_title');
          $query->condition('ft.entity_id', $current_nid, '<>');
          $query->condition('td.tid', $terms, 'IN');
          $result = $query->execute();

          while ($row = $result->fetchObject()) {
            if (!in_array($row->entity_id, $visited_nodes)) {
              $field_info = field_info_instance('node', $field_name, $row->bundle);
              $field_label = $field_info['label'];
              // Create a new graph relation.
              $new_rel = array(
                'nid' => $current_nid,
                'target' => $row->tid,
                'name' => t($field_label),
                'type' => 'taxonomy',
                'color' => $info['color'],
                'source_title' => t($row->source_title),
                'target_title' => t($row->name),
              );
              array_push($relations, $new_rel);
              if (!cccgraph_in_array($row->entity_id, $novisited_nodes, $pp)) {
                $novisited_nodes[count($novisited_nodes)] = $row->entity_id;
              }
            }
          }
        }
      }
    }
    $visited_nodes[count($visited_nodes)] = $novisited_nodes[$pp];
    $pp++;
    if ($next_level == $pp) {
      $next_level = count($novisited_nodes);
      $level++;
    }
  }

  // Extract relations.
  foreach ($relations as $rel) {
    if ($rel['type'] == 'node_reference') {
      $directed = 'true';
      $edges[] = array(
        'source' => $rel['source_title'],
        'target' => $rel['target_title'],
        'type' => 'licensing',
        'directed' => $directed,
      );
    }
    else {
      $directed = 'false';
      $edges[] = array(
        'source' => $rel['source_title'],
        'target' => $rel['target_title'],
        'type' => 'resolved',
        'directed' => $directed,
      );
    }
  }

  return $edges;
}

/**
 * Auxiliary function.
 *
 * Search needle in haystack from the position pos.
 */
function cccgraph_in_array($needle, $haystack, $pos) {

  for ($i = $pos; $i < count($haystack); $i++) {
    if ($haystack[$i] == $needle) {
      return TRUE;
    }
  }
  return FALSE;
}
