<?php

/**
 * @file
 * Admin callbacks for cccgraph module.
 */

/**
 * Configuration settings form.
 */
function cccgraph_admin() {

  $library = libraries_detect('d3');
  $error_message = isset($library['error message']) ? $library['error message'] : '';
  if ($error_message) {
    drupal_set_message(t('!error', array('!error' => $error_message)), 'error');
  }

  $form['cccgraph_width'] = array(
    '#type' => 'textfield',
    '#title' => t('SVG width'),
    '#default_value' => variable_get('cccgraph_width', '720'),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t("Width of the SVG element displayed in the block"),
    '#access' => user_access('administer_cccgraph'),
  );
  $form['cccgraph_height'] = array(
    '#type' => 'textfield',
    '#title' => t('SVG height'),
    '#default_value' => variable_get('cccgraph_height', '540'),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t("Height of the SVG element displayed in the block"),
    '#access' => user_access('administer_cccgraph'),
  );
  $form['cccgraph_levels'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph depth'),
    '#default_value' => variable_get('cccgraph_levels', '2'),
    '#size' => 4,
    '#maxlength' => 1,
    '#description' => t("Relations depth that will be displayed"),
  );
  $form['current_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Current node'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['current_node']['cccgraph_current_color'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 7,
    '#title' => t('Color'),
    '#default_value' => variable_get('cccgraph_current_color', 'Yellow'),
    '#description' => t("Color of the node representing the current page. Use a hexadecimal format e.g. #FF0000"),
  );
  $form['current_node']['cccgraph_current_color_fill'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 7,
    '#title' => t('Fill color'),
    '#default_value' => variable_get('cccgraph_current_fillcolor', 'Yellow'),
    '#description' => t("Color of the node representing the current page. Use a hexadecimal format e.g. #FF0000"),
  );
  $form['current_node']['cccgraph_current_color_text'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 7,
    '#title' => t('Text color'),
    '#default_value' => variable_get('cccgraph_current_color_text', 'Black'),
    '#description' => t("Text color of the node representing the current page. Use a hexadecimal format e.g. #FF0000"),
  );

  // @TODO: insert validations
  return system_settings_form($form);
}

/**
 * Create the form for administer fields than will be represented in the graph.
 */
function cccgraph_content() {

  $nodes_color = json_decode(variable_get('cccgraph_nodes_color', ''), TRUE);
  $visible_fields = array();
  $visible_fields['taxonomy_term_reference'] = json_decode(variable_get('cccgraph_visible_taxonomies', ''), TRUE);
  $visible_fields['node_reference'] = json_decode(variable_get('cccgraph_visible_nodereferences', ''), TRUE);

  $nodes = db_query('SELECT name, type FROM {node_type}');
  while ($row = $nodes->fetchObject()) {
    // Creates the form fieldset for every existing content type.
    $form[$row->type] = array(
      '#type' => 'fieldset',
      '#title' => $row->name,
      '#collapsible' => TRUE,
    );
    $default_value = '#000000';
    if (!empty($nodes_color[$row->type]['color'])) {
      $default_value = $nodes_color[$row->type]['color'];
    }
    $form_field = implode('-', array('content', $row->type));
    $form[$row->type][$form_field] = array(
      '#type' => 'textfield',
      '#title' => 'Color for <em>' . $row->name . '</em> nodes',
      '#size' => 10,
      '#maxlength' => 7,
      '#default_value' => $default_value,
    );
    // Get existing node_reference fields.
    $f_relations = db_query("SELECT fc.field_name, fc.cardinality, fc.type, fci.entity_type, fci.bundle 
                              FROM {field_config} fc JOIN {field_config_instance} fci ON fc.field_name = fci.field_name 
                              WHERE fci.bundle = :type and (type='node_reference' or type = 'taxonomy_term_reference')",
                              array(':type' => $row->type)
                            );

    while ($row1 = $f_relations->fetchObject()) {
      // Create the node_reference and taxonomy form fields.
      $info = field_info_instance($row1->entity_type, $row1->field_name, $row1->bundle);
      $label = $info['label'];
      $default_value = '';
      if (!empty($visible_fields[$row1->type][$row->type][$row1->field_name]['color'])) {
        $default_value = $visible_fields[$row1->type][$row->type][$row1->field_name]['color'];
      }
      if ($row1->type == 'taxonomy_term_reference') {
        $field_info = array('taxonomy', $row->type, $row1->field_name);
        $form_field = implode('-', $field_info);
      }
      else {
        $field_info = array('reference', $row->type, $row1->field_name);
        $form_field = implode('-', $field_info);
      }
      $form[$row->type][$form_field] = array(
        '#type' => 'textfield',
        '#title' => $label,
        '#size' => 10,
        '#maxlength' => 7,
        '#default_value' => $default_value,
      );
      if ($row1->type == 'taxonomy_term_reference') {
        $form[$row->type][$form_field] += array(
          '#description' => t('Relation by taxonomy'),
        );
      }
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Handle the fields selected in the form.
 */
function cccgraph_content_submit($form, $form_state) {

  $nodes = $reference = $taxonomy = array();

  foreach ($form_state['values'] as $field_key => $field_value) {
    $field = explode('-', $field_key);
    $element = $field[0];
    if (isset($field[1])) {
      $content_type = $field[1];
      if ($element == 'content') {
        $nodes[$content_type]['color'] = $field_value;
      }
      elseif ($element == 'reference' || $element == 'taxonomy') {
        $field_name = $field[2];
        $field_data = array(
          'color' => $field_value,
          'cardinality' => '-1',
        );
        ${$element}[$content_type][$field_name] = $field_data;
      }
    }
  }

  variable_set('cccgraph_nodes_color', json_encode($nodes));
  variable_set('cccgraph_visible_nodereferences', json_encode($reference));
  variable_set('cccgraph_visible_taxonomies', json_encode($taxonomy));
}
